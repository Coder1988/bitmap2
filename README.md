## Bitmap Library

This is going to be an extended version of my lite version bitmap header. It should be a pretty in-depth bitmap handler library. I will be updated extensively.

### Examples
 - test1.c - Creates a test bitmap named test.bmp with black background.

 - test2.c - Create a steganograph from a text file into an existing bitmap.

 - test3.c - Display steganograph hidden inside bitmap image.

 - test4.c - Create a test bitmap; utilise draw_circle_bitmap() function.

 - test5.c - Utilize the randomise_bitmap() function.

 - test6.c - Make ASCII art from the given bitmap file. [WIP]

### Developer

 - Philip R. Simonson

## License

 - None (ATM)
