#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bitmap.h"

/* program to demonstrate color conversion to greyscale */
int
main (int argc, char **argv)
{
	Bitmap *bmp;

	if (argc != 2) {
		printf("Usage: %s <bmpfile>\n", argv[0]);
		return 1;
	}
	bmp = load_bitmap(argv[1]);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;

	bitmap_to_greyscale(bmp);
	write_bitmap(bmp, argv[1]);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;
	destroy_bitmap(bmp);
	return 0;
}
