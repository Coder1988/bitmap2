#include <stdio.h>
#include <string.h>
#include "bitmap.h"

/* program to create a bitmap and draw a blue circle on black background */
int
main ()
{
	Color pixel = {0,0,255};
	Bitmap *bmp;
	
	bmp = create_bitmap(640, 480);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;

	memset(bmp->data, 0, bmp->info.isize);
	draw_circle_bitmap(bmp, 0, 0, 100, pixel);
/*	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;
*/
	write_bitmap(bmp, "test.bmp");
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;

	destroy_bitmap(bmp);
	return 0;
}
