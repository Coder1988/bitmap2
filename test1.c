#include <stdio.h>
#include <string.h>
#include "bitmap.h"

/* program to create a blank bitmap image with black background */
int
main ()
{
	Bitmap *bmp;

	bmp = create_bitmap(800, 600);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;
	memset(bmp->data, 0, bmp->info.isize);
	write_bitmap(bmp, "test.bmp");
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;
	destroy_bitmap(bmp);
	return 0;
}
