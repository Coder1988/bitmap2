#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bitmap.h"

#define PIXEL_STEP 1		/* step; skip how many pixels */
#define ASCIIART "#&$/ "	/* ASCII characters */

/* blank enumeration for ASCII characters */
enum {
	POUND,
	ANPERS,
	DOLLAR,
	SLASH,
	BLANK
};

/* bitmap_to_ascii:  convert a bitmap to ascii art. */
void
bitmap_to_ascii (Bitmap *bmp)
{
	int average;
	int x,y;

	for(y=0; y<bmp->info.height; y+=PIXEL_STEP) {
		for(x=0; x<bmp->info.width; x+=PIXEL_STEP) {
			Color pixel;

			get_pixel_bitmap(bmp, y, x, &pixel);
			average = (pixel.r + pixel.g + pixel.b) / 3;

			if (average >= 200 && average < 256) {
				putchar(ASCIIART[BLANK]);
			} else if (average >= 100 && average < 200) {
				putchar(ASCIIART[ANPERS]);
			} else if (average >= 50 && average < 100) {
				putchar(ASCIIART[DOLLAR]);
			} else if (average >= 25 && average < 50) {
				putchar(ASCIIART[SLASH]);
			} else {
				putchar(ASCIIART[POUND]);
			}
		}
		putchar('\n');
	}
}

/* program to demonstrate ascii art conversion */
int
main (int argc, char **argv)
{
	Bitmap *bmp;

	if (argc != 2) {
		printf("Usage: %s <bmpfile>\n", argv[0]);
		return 1;
	}
	bmp = load_bitmap(argv[1]);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;

	bitmap_to_ascii(bmp);
	destroy_bitmap(bmp);
	return 0;
}
